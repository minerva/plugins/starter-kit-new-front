let $ = require('jquery');

require('../css/styles.css');
require('./minervaAPI');

/* globals minerva:MinervaAPI */

const pluginName = 'starter-kit';
const pluginVersion = '2.0.0';
const pluginUrl = 'https://lux1.atcomp.pl/plugins/starter-kit-new-front/plugin.js';

const containerName = pluginName + '-container';

const globals = {
  selected: [],
  allBioEntities: [],
  pickedRandomly: undefined
};


// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let pluginContainer;
let minervaVersion;

let minervaApiUrl;
let newMinervaApiUrl;
let addListener;
let removeAllListeners;

/**
 *
 * @type {MinervaElement[]}
 */
let allElements = [];
/**
 *
 * @type {MinervaReaction[]}
 */
let allReactions = [];

/**
 *
 * @param {string} url
 * @return {Promise<string|object>}
 */
function getPageContent(url) {
  return new Promise(async function (resolve) {
    const content = (await (await fetch(url)).text())
    resolve(content);
  });
}

/**
 *
 * @param {string} url
 * @return {Promise<any>}
 */
function getJsonPageContentAsObject(url) {
  return getPageContent(url).then(function (content) {
    let data = content;
    if (typeof content === 'string' || content instanceof String) {
      data = JSON.parse(content);
    }
    return data;
  });
}

const register = async function () {

  console.log('registering ' + pluginName + ' plugin');

  //minerva plugin API for versions below 18 is not supported (there was different registration process in old API)
  if (minerva.plugins === undefined || minerva.plugins.registerPlugin === undefined) {
    alert('Minerva version not supported. Required version 18.0 or later');
    return;
  }

  let pluginData = minerva.plugins.registerPlugin({
    pluginName,
    pluginVersion,
    pluginUrl,
  });
  addListener = pluginData.events.addListener;
  removeAllListeners = pluginData.events.removeAllListeners;

  pluginContainer = $(pluginData.element);

  console.log('project id: ', minerva.project.data.getProjectId());

  minervaApiUrl = minerva.project.data.getApiUrls().baseApiUrl;
  newMinervaApiUrl = minerva.project.data.getApiUrls().baseNewApiUrl;
  let url = minervaApiUrl + "/" + "configuration/";

  let configuration = await getJsonPageContentAsObject(url)
  minervaVersion = parseFloat(configuration.version.split('.').slice(0, 2).join('.'));

  console.log('minerva version: ', minervaVersion);
  await initPlugin();
};

register();

const unregister = async function () {
  console.log('unregistering ' + pluginName + ' plugin');

  unregisterListeners();
  await deHighlightAll();

  allElements = [];
  allReactions = [];
};


function initPlugin() {
  registerListeners();
  initMainPageStructure();
}

function registerListeners() {
  addListener("onSearch", searchListener);
  addListener("onPluginUnload", unregister);
}

function unregisterListeners() {
  removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll() {
  minerva.data.bioEntities.removeAllMarkers();
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function initMainPageStructure() {

  const container = $(`<div class=${containerName}></div>`).appendTo(pluginContainer);
  container.append(`
        <div class="panel panel-default card panel-events mb-2">
            <div class="panel-heading card-header">Events (Select an element in the map)</div>
            <div class="panel-body card-body">                
            </div>
        </div>
    `);
  container.append('<button type="button" class="btn-focus btn btn-primary btn-default btn-block">Focus</button>');
  container.append('<button type="button" class="btn-highlight btn btn-primary btn-default btn-block">Highlight (icon)</button>');

  container.append('<hr>');
  container.append('<button type="button" class="btn-pick-random btn btn-primary btn-default btn-block">Retrieve random object from map</button>');
  container.append(`
        <div class="panel panel-default card panel-randomly-picked mb-2">
            <div class="panel-heading card-header">Randomly picked object</div>
            <div class="panel-body card-body">                
            </div>
        </div>
    `);
  container.append('<button type="button" class="btn-focus-random btn btn-primary btn-default btn-block">Focus</button>');
  container.append('<button type="button" class="btn-highlight-random btn btn-primary btn-default btn-block">Highlight (surface)</button>');

  container.append('<hr>');
  container.append('<h4>Query UniProt API</h4>');
  container.append('<button type="button" class="btn-uniprot btn btn-primary btn-default btn-block" ' +
    'title="Queries UniProt using the element selected from the map">Retrieve from UniProt</button>');
  container.append(`
        <div class="panel panel-default card panel-uniprot">
            <div class="panel-heading card-header">Uniprot records for the selected element</div>
            <div class="panel-body card-body">
                <pre><code></code></pre>
            </div>
        </div>
    `);

  container.find('.btn-highlight').on('click', () => highlightSelected());
  container.find('.btn-focus').on('click', () => focusOnSelected());
  container.find('.btn-pick-random').on('click', () => pickRandom());
  container.find('.btn-highlight-random').on('click', () => highlightSelected(true));
  container.find('.btn-focus-random').on('click', () => focusOnSelected(true));
  container.find('.btn-uniprot').on('click', () => retrieveUniprot());
}

function isMinervaElement(bioEntity) {
  // elements have z index (reactions does not have it)
  return "glyph" in bioEntity;
}

/**
 * @param {MinervaSearchListenerData} data
 */
function searchListener(data) {
  globals.selected = data.results[0];

  let str = '';
  if (globals.selected.length > 0) {
    globals.selected.forEach(e => {
      let bioEntity = e.bioEntity;
      if (!bioEntity) {
        bioEntity = e;
      }
      // check if it's an element
      if (isMinervaElement(bioEntity)) {
        str += `<div>${bioEntity.name} - ${bioEntity.id}</div>`
      }
    });
  }
  pluginContainer.find('.panel-events .panel-body').html(str);
}

async function pickRandom() {

  async function pick() {
    /**
     *
     * @type {*[]}
     */
    let allBioEntities = allReactions;
    allBioEntities = allBioEntities.concat(allElements);

    let picked = allBioEntities[Math.floor(Math.random() * allBioEntities.length)];
    let html = '';
    if (isMinervaElement(picked)) {
      let url = newMinervaApiUrl + "/" + "projects/" + minerva.project.data.getProjectId() + "/models/" + picked.modelId + "/bioEntities/elements/" + picked.id;
      //fetch data using new API
      globals.pickedRandomly = await getJsonPageContentAsObject(url);

      html += `Element: <div>${globals.pickedRandomly.name} - ${globals.pickedRandomly.id}</div>`
    } else {
      let url = newMinervaApiUrl + "/" + "projects/" + minerva.project.data.getProjectId() + "/models/" + picked.modelId + "/bioEntities/reactions/" + picked.id;
      //fetch data using new API
      globals.pickedRandomly = await getJsonPageContentAsObject(url);
      html += `Reaction: <div>${globals.pickedRandomly.idReaction}</div>`
    }

    pluginContainer.find('.panel-randomly-picked .panel-body').html(html);
  }

  if (allElements.length + allReactions.length > 0) {
    await pick();
  } else {
    let url = minervaApiUrl + "/" + "projects/" + minerva.project.data.getProjectId() + "/models/*/bioEntities/elements/";
    allElements = await getJsonPageContentAsObject(url);

    url = minervaApiUrl + "/" + "projects/" + minerva.project.data.getProjectId() + "/models/*/bioEntities/reactions/";
    allReactions = await getJsonPageContentAsObject(url);
    await pick();
  }
}

/**
 *
 * @param {MinervaElement} element
 * @return {MarkerData}
 */
function elementToSurfaceData(element) {
  return {
    id: "E" + element.id,
    modelId: element.modelId ? element.modelId : element.model,
    type: "surface",
    color: '#00FF00',
    opacity: 0.7,
    x: element.x,
    y: element.y,
    width: element.width,
    height: element.height
  }
}

/**
 *
 * @param {MinervaReaction} reaction
 * @return {MarkerData[]}
 */
function reactionToLinesData(reaction) {
  let linesData = [];
  let line = reaction.line;
  let modelId = reaction.modelId ? reaction.modelId : reaction.model;
  let segmentsData = segmentsToLinesData(line.segments, modelId);
  linesData = linesData.concat(segmentsData);
  for (let i = 0; i < reaction.operators.length; i++) {
    let operator = reaction.operators[i];
    segmentsData = segmentsToLinesData(operator.line.segments, modelId);
    linesData = linesData.concat(segmentsData);
  }
  for (let i = 0; i < reaction.modifiers.length; i++) {
    let modifier = reaction.modifiers[i];
    segmentsData = segmentsToLinesData(modifier.line.segments, modelId);
    linesData = linesData.concat(segmentsData);
  }
  for (let i = 0; i < reaction.products.length; i++) {
    let product = reaction.products[i];
    segmentsData = segmentsToLinesData(product.line.segments, modelId);
    linesData = linesData.concat(segmentsData);
  }
  for (let i = 0; i < reaction.reactants.length; i++) {
    let reactant = reaction.reactants[i];
    segmentsData = segmentsToLinesData(reactant.line.segments, modelId);
    linesData = linesData.concat(segmentsData);
  }
  return linesData;
}

let counter = 0;

/**
 *
 * @param {MinervaSegment[]} segments
 * @param {number} modelId
 * @return {MarkerData[]}
 */
function segmentsToLinesData(segments, modelId) {
  let linesData = [];
  for (let i = 0; i < segments.length; i++) {
    let segment = segments[i];
    linesData.push({
      id: "S" + counter++,
      modelId: modelId,
      type: "line",
      color: '#FF0000',
      opacity: 0.7,
      start: {
        x: segment.x1,
        y: segment.y1,
      },
      end: {
        x: segment.x2,
        y: segment.y2,
      }
    });
  }
  return linesData;
}

/**
 *
 * @param {MinervaElement} element
 * @return {MarkerData}
 */
function elementToPinData(element) {
  return {
    id: "E" + element.id,
    modelId: element.modelId ? element.modelId : element.model,
    type: "pin",
    color: '#00FF00',
    opacity: 0.7,
    x: element.x + element.width / 2,
    y: element.y + element.height / 2,
  }
}

function highlightSelected(pickedRandomly = false) {

  const highlightDefs = [];

  if (pickedRandomly) {
    if (globals.pickedRandomly) {
      if (isMinervaElement(globals.pickedRandomly)) {
        highlightDefs.push(elementToSurfaceData(globals.pickedRandomly))
      } else {
        let linesData = reactionToLinesData(globals.pickedRandomly);
        for (let i = 0; i < linesData.length; i += 1) {
          highlightDefs.push(linesData[i]);
        }
      }
    }
  } else {
    globals.selected.forEach(selected => {
      let bioEntity = selected.bioEntity;
      if (!bioEntity) {
        bioEntity = selected;
      }
      if (isMinervaElement(bioEntity)) {
        highlightDefs.push(elementToPinData(bioEntity))
      } else {
        let linesData = reactionToLinesData(bioEntity);
        for (let i = 0; i < linesData.length; i += 1) {
          highlightDefs.push(linesData[i]);
        }
      }
    });
  }

  highlightDefs.forEach(markerData => {
    minerva.data.bioEntities.addSingleMarker(markerData)
  });
}

function focusOnSelected(pickedRandomly = false) {

  function focus(entity) {
    minerva.map.openMap({id: entity.modelId ? entity.modelId : entity.model});
    if (isMinervaElement(entity)) {
      minerva.map.fitBounds({
        x1: entity.x,
        y1: entity.y,
        x2: entity.x + entity.width,
        y2: entity.y + entity.height
      });
    } else {
      let x1 = Math.min(entity.line.segments[0].x1, entity.line.segments[0].x2);
      let x2 = Math.max(entity.line.segments[0].x1, entity.line.segments[0].x2);
      let y1 = Math.min(entity.line.segments[0].y1, entity.line.segments[0].y2);
      let y2 = Math.max(entity.line.segments[0].y1, entity.line.segments[0].y2);
      entity.line.segments.forEach(segment => {
        x1 = Math.min(Math.min(segment.x1, segment.x2), x1);
        y1 = Math.min(Math.min(segment.y1, segment.y2), y1);
        x2 = Math.max(Math.max(segment.x1, segment.x2), x2);
        y2 = Math.max(Math.max(segment.y1, segment.y2), y2);
      })
      minerva.map.fitBounds({
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2
      });
    }
  }

  if (!pickedRandomly && globals.selected.length > 0) {
    focus(globals.selected[0].bioEntity);
  } else if (pickedRandomly && globals.pickedRandomly) {
    focus(globals.pickedRandomly);
  }
}

function retrieveUniprot() {
  let query = pluginContainer.find('.panel-events .panel-body').text();
  query = query.substring(0, query.indexOf(' - '));
  $.ajax({
    type: 'GET',
    url: 'https://rest.uniprot.org/uniprotkb/search?&format=tsv&query=' + query
  }).then(function (result) {
    pluginContainer.find('.panel-uniprot .panel-body code').text(result);
  })
}